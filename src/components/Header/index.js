import React from "react";
import "./Header.css";
function Header({ title }) {
  return (
    <header>
      <h1>{title || "Header title"}</h1>
    </header>
  );
}

export default Header;
