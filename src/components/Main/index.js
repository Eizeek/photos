import React from "react";
import Photos from "../Photos/Photos";
import "./Main.css";
function Main({ photos }) {
  return (
    <main>
      <Photos photos={photos} />
    </main>
  );
}

export default Main;
