import React from "react";
import "./Footer.css";
function Footer({ title }) {
  return (
    <footer>
      <p>{title || "Footer title"}</p>
    </footer>
  );
}

export default Footer;
